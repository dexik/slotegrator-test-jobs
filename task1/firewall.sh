#!/usr/bin/env bash

IPT=$(which iptables)
RSYSLOG_CONF="/etc/rsyslog.d/10-iptables.conf"
LOGROTATE_CONF="/etc/logrotate.d/iptables"
CHAINS=(
  "allow_all"
  "db_app_allow_all"
  "users_allow_all"
  "tmp_users_allow"
  "external_service"
)

OTHER_PREFS=(
  "iptables_deny_other"
  "iptables_deny_invalid"
)

main() {

  $IPT -F
  $IPT -X

  apt-get -y install rsyslog
  systemctl --now enable rsyslog
  rm -f $RSYSLOG_CONF

  for _chain in ${CHAINS[@]}; do
    $IPT -N ${_chain}
    echo ":msg, contains, \"iptables_${_chain}: \" -/var/log/iptables/${_chain}.log" >> ${RSYSLOG_CONF}
  done

  for _chain in ${OTHER_PREFS[@]}; do
    echo ":msg, contains, \"iptables_${_chain}: \" -/var/log/iptables/${_chain}.log" >> ${RSYSLOG_CONF}
  done

  cat <<EOD > ${LOGROTATE_CONF}
/var/log/iptables/*.log
{
        rotate 7
        daily
        missingok
        notifempty
        delaycompress
        compress
        postrotate
                /usr/lib/rsyslog/rsyslog-rotate
        endscript
EOD

  $IPT -A INPUT -m conntrack --ctstate INVALID -j LOG --log-prefix "iptables_deny_invalid: "
  $IPT -A INPUT -m conntrack --ctstate INVALID -j DROP
  $IPT -A INPUT -i lo -j ACCEPT
  $IPT -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
  $IPT -A INPUT -p icmp -j ACCEPT

  $IPT -A allow_all -s 192.168.0.0/24 -m comment --comment "our gold clients" -j ACCEPT
  $IPT -A allow_all -s 192.168.3.0/24 -m comment --comment "our platinum clients" -j ACCEPT
  $IPT -A allow_all -s 185.42.43.200/32 -m comment --comment "our silver clients" -j ACCEPT
  $IPT -A allow_all -j LOG --log-prefix "iptables_allow_all: "
  $IPT -A allow_all -j RETURN

  $IPT -A db_app_allow_all -s 10.10.0.0/24 -m comment --comment "our database and app servers amsterdam subnet" -j ACCEPT
  $IPT -A db_app_allow_all -j LOG --log-prefix "iptables_db_app_allow_all: "
  $IPT -A db_app_allow_all -j RETURN

  $IPT -A users_allow_all -s 95.104.193.150/32 -m comment --comment "db admin Petya, Jira task: DB-5681" -j ACCEPT
  $IPT -A users_allow_all -j LOG --log-prefix "iptables_users_allow_all: "
  $IPT -A users_allow_all -j RETURN

  $IPT -A tmp_users_allow -s 95.104.193.100/32 -m comment --comment "db admin Vasya, Jira task: DB-5682, will delete by at job on 1 May 2021" -j ACCEPT
  $IPT -A tmp_users_allow -j LOG --log-prefix "iptables_tmp_users_allow: "
  $IPT -A tmp_users_allow -j RETURN

  $IPT -A external_service -p tcp --dport 80 -m comment --comment "nginx http port" -j ACCEPT
  $IPT -A external_service -p tcp --dport 443 -m comment --comment "nginx https port" -j ACCEPT
  $IPT -A external_service -j LOG --log-prefix "iptables_external_service: "
  $IPT -A external_service -j RETURN

  $IPT -A INPUT -j allow_all
  $IPT -A INPUT -j db_app_allow_all
  $IPT -A INPUT -j users_allow_all
  $IPT -A INPUT -p tcp -m multiport --dports 8080,8081 -j tmp_users_allow
  $IPT -A INPUT -j external_service
  $IPT -A INPUT -j LOG --log-prefix "iptables_deny_other: "
  $IPT -A INPUT -p tcp -j REJECT --reject-with tcp-reset
  $IPT -A INPUT -p udp -j REJECT --reject-with icmp-port-unreachable
  $IPT -A INPUT -p icmp -j REJECT --reject-with icmp-port-unreachable

  $IPT -P INPUT DROP

  mkdir -p /var/log/iptables
  systemctl restart rsyslog

}

main
