#!/usr/bin/env bash

#FLOCK_BIN=$(which flock)
#[ "${FLOCKER}" != "$0" ] && exec env FLOCKER="$0" $FLOCK_BIN -en "$0" "$0" "$@" || :

# --- args ---
ARGS=("$@")
BASH_SELF=$(/usr/bin/basename "$BASH_SOURCE")

# --- options ---
OPT_SSH_USER="root"
OPT_STORE_DIR=""
OPT_REMOTE_HOST=""
OPT_DEBUG="0"
OPT_REMOTE_DIR=""
OPT_TYPE="full"

# --- consts ---
CONST_TAR_SNAP="/var/tmp/backup.snap"
CONST_GPG_EMAIL="admin@ircclub.net"

# --- vars ---

# --- help ---
usage() {
    message=(
        "${BASH_SELF} custom backup script"
        ""
        "Usage:"
        " ${BASH_SELF} [options]"
        ""
        "Options:"
        " -u, --user\t\t\tSSH user (Deafult: root)"
        " -s, --store\t\t\tdirectory for backups"
        " -r, --remotehost\t\tremote host"
        " -d, --debug\t\t\tdebug (Default: disabled)"
        " -k, --remotedir\t\tremote dir"
        " -t, --type\t\t\tbackup type (Default: full)"
        )
        printf '%b\n' "${message[@]}"
        exit
}

# --- args parsing ---
options() {
    OPTS=$(/usr/bin/getopt \
        --options "u:s:r:dk:t:h" \
        --longoptions "user:,store:,remotehost:,debug,remotedir:,type:,help" \
        --name "${BASH_SELF}" \
        -- "${ARGS[@]}"
    )

    eval set -- "$OPTS"

    while [[ $# -gt 0 ]]; do
        case "$1" in
            -u|--user)
                OPT_SSH_USER=$2
                shift 2
                ;;
            -s|--store)
                OPT_STORE_DIR=$2
                shift 2
                ;;
            -r|--remotehost)
                OPT_REMOTE_HOST=$2
                shift 2
                ;;
            -d|--debug)
                OPT_DEBUG="1"
                shift 1
                ;;
            -k|--remotedir)
                OPT_REMOTE_DIR="$OPT_REMOTE_DIR $2"
                shift 2
                ;;
            -t|--type)
                OPT_TYPE=$2
                shift 2
                ;;
            -h|--help)
	        usage
		exit
                ;;
	    *)
	        shift
		;;
        esac
    done
}

doBackup() {
    local _verbose
    local _level

    case "$OPT_DEBUG" in
        "0")
            _verbose=""
            ;;
        "1")
            _verbose="-v"
            ;;
    esac

    case "$OPT_TYPE" in
        "full")
            _level="--level=0"
            ;;
        "inc")
            _level=""
            ;;
    esac

    mkdir -p $OPT_STORE_DIR/$OPT_REMOTE_HOST/$OPT_TYPE $OPT_STORE_DIR/$OPT_REMOTE_HOST/$OPT_TYPE"_Old"
    if [ -f "$OPT_STORE_DIR/$OPT_REMOTE_HOST/$OPT_TYPE/backup.tar.gz.gpg" ]; then
        printf "%s\n" "Unfortunately, last backup have not rotated yet. I cannot continue :(((."
        exit 1
    fi

    ssh $_verbose $OPT_SSH_USER@$OPT_REMOTE_HOST \
        "tar $_verbose $_level -g $CONST_TAR_SNAP -zcf - $OPT_REMOTE_DIR | \
        gpg -r $CONST_GPG_EMAIL --encrypt" > $OPT_STORE_DIR/$OPT_REMOTE_HOST/$OPT_TYPE/backup.tar.gz.gpg

    # Need sure that your backup has been made without any errors
    if [[ ! $? == 0 ]]; then 
        rm -f $OPT_STORE_DIR/$OPT_REMOTE_HOST/$OPT_TYPE/backup.tar.gz.gpg
        exit 1
    fi
}

main() {
    local _opt
    options
    # check for necessary options
    for _opt in "$OPT_STORE_DIR" \
                "$OPT_REMOTE_HOST" \
                "$OPT_REMOTE_DIR"; do
        [[ -z "$_opt" ]] && usage
    done
    # check for backup type
    [[ "$OPT_TYPE" =~ ^(full|inc)$ ]] || usage

    doBackup

}

main
